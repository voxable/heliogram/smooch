# frozen_string_literal: true

module Heliogram
  module Smooch
    class PostbackWorker < Heliogram::Smooch::Worker
      include Sidekiq::Worker
      # TODO: Make number of retries configurable.
      sidekiq_options retry: 1

      # Process an inbound postback.
      #
      # @param [String, Integer] user_id
      #   The ID representing the Smooch user.
      # @param app_id [String]
      #   The ID representing the Smooch app..
      # @param [String] redis_namespace
      #   The redis namespace under which the postback to process is nested.
      # @param [String] platform
      #   The messaging platform for this postback.
      #
      # @return [void]
      def perform(user_id, app_id, redis_namespace, platform)
        # Retrieve the latest postback payload for this user
        payload = pop_raw_postback(user_id, redis_namespace)

        # Do nothing if no postback available. This could be due to multiple
        # execution on the part of Sidekiq. This ensures idempotence. We loop
        # here to ensure that this worker attempts to drain the queue for
        # the user.
        until payload.empty?
          # Locate the class representing the bot.
          bot = Heliogram::Smooch.bots[app_id]

          # Fetch the User representing the message's sender
          # TODO: pass in a `user_id_field` to indicate how to find user in order to
          # make this platform agnostic
          user = find_bot_user(bot, user_id)

          # Build the request object.
          request = build_payload_request(payload, platform, user, app_id)

          # TODO: High - needs to be a callback (message pipeline)
          Appsignal.tag_job(
            payload: payload.to_s.truncate(100)
          )
          # Send the request to the bot's router.
          bot.router.handle(request)

          # Record the user postback with analytics services.
          bot.record_postback(
            postback: payload,
            user_id:  user_id,
            platform: platform
          )

          # Attempt to pop another postback from the queue for processing.
          payload = pop_raw_postback(user_id, redis_namespace)
        end
      end

      # @return [Hash] The latest raw postback from this user's queue.
      private def pop_raw_postback(user_id, redis_namespace)
        pop_from_queue(
          Heliogram::Smooch::Queues::PostbackQueue,
          user_id:   user_id,
          namespace: redis_namespace
        )
      end
    end
  end
end
