# frozen_string_literal: true

module Heliogram
  module Smooch
    class Worker < Heliogram::Worker

      # Build a request object from a payload and user.
      #
      # @param [Hash] payload
      #   The postback payload.
      # @param [String] platform
      #   The messaging platform for this request.
      # @param [Object] user
      #   The user that sent the payload.
      # @param [String] app_id
      #   The ID of the Smooch app handling the message.
      #
      # @return [Heliogram::Smooch::Request]
      #   The generated request.
      private def build_payload_request(payload, platform, user, app_id)
        # Generate the params hash.
        parameters = payload['parameters'] || payload['params']

        # Build a request object.
        Heliogram::Smooch::Request.new(
          user:       user,
          intent:     payload['intent'],
          action:     payload['action'],
          parameters: parameters,
          platform:   platform,
          app_id:     app_id
        )
      end
    end
  end
end
