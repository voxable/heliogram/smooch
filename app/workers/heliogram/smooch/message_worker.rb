# frozen_string_literal: true

module Heliogram
  module Smooch
    class MessageWorker < Heliogram::Smooch::Worker
      # TODO: Make number of retries configurable.
      sidekiq_options retry: 1

      # Process an inbound message.
      #
      # @param [String, Integer] user_id
      #   The ID representing the Smooch user.
      # @param [String] app_id
      #   The ID representing the Smooch app.
      # @param [String] redis_namespace
      #   The redis namespace under which the message to process is nested.
      #
      # @return [void]
      def perform(user_id, app_id, redis_namespace)
        # Retrieve the latest message for this user.
        raw_message = pop_raw_message(user_id, redis_namespace)

        # Do nothing if no message available. This could be due to multiple
        # execution on the part of Sidekiq. This ensures idempotence. We loop
        # here to ensure that this worker attempts to drain the queue for
        # the user.
        until raw_message.empty?
          # Instantiate a message object with the raw message from the queue.
          message = SmoochApi::Message.new(raw_message)

          # Locate the class representing the bot.
          bot = Heliogram::Smooch.bots[app_id]

          # Fetch the User representing the message's sender.
          # TODO: pass in a `user_id_field` to indicate how to find user
          # make this platform agnostic
          user = find_bot_user(bot, user_id)

          # Determine the message's platform.
          platform = message.source['type']

          # If there is a payload, build a payload request.
          if message.payload
            user_message = JSON.parse(message.payload)

            request = build_payload_request(
              user_message, platform, user, app_id
            )

            # Record analytics info.
            intent      = request.action
            not_handled = false
          # TODO: dialog_action should be constantized
          elsif dialog_action = user.context['dialog_action']
            user_message = message.text

            request = build_dialog_request(message, platform, user, app_id)

            # Record analytics info.
            intent      = dialog_action
            not_handled = false
          # Otherwise, parse the user message.
          else
            user_message = message.text
            nlu_response = parse_message(
              message.text, user_id, user
            )

            # Build a request.
            request = build_request(
              message:      message,
              platform:     platform,
              nlu_response: nlu_response,
              params:       nlu_response[:parameters],
              user:         user,
              app_id:       app_id
            )

            # Record analytics info.
            if request.action == InternalActions::DEFAULT
              intent      = nil
              not_handled = true
            else
              intent      = request.action
              not_handled = false
            end
          end

          # TODO: High - needs to be a callback (message pipeline)
          Appsignal.tag_job(
            message: user_message.to_s.truncate(100)
          )

          # Send the request to the bot's router.
          bot.router.handle(request) if request

          # Record the user message with analytics services.
          bot.record_message(
            message:     message,
            intent:      intent,
            entities:    request&.params,
            not_handled: not_handled,
            user_id:     user_id,
            platform:    platform,
            timestamp:   message.received
          )

          # Attempt to pop another message from the queue for processing.
          raw_message = pop_raw_message(user_id, redis_namespace)
        end
      end

      # Build a request when the user is in the midst of a dialog prompt.
      #
      # @param [Hash] message
      #   The message for this request.
      # @param [String] platform
      #   The messaging platform for this request.
      # @param [Object] user
      #   The user for this request.
      # @param [String] app_id
      #   The Smooch app id for the bot handling this request.
      #
      # @return [Heliogram::Smooch::Request]
      #   The generated dialog request.
      private def build_dialog_request(message, platform, user, app_id)
        # Fetch the information from the user's context.
        action       = user.context['dialog_action']
        parameters   = user.context['dialog_parameters']

        # Build a request object.
        request = Heliogram::Smooch::Request.new(
          app_id:     app_id,
          user:       user,
          message:    message,
          platform:   platform,
          intent:     action,
          action:     action,
          parameters: parameters
        )

        # Clear the user's dialog context.
        user.update_context!(dialog_action: nil)
        user.update_context!(dialog_parameters: nil)

        request
      end

      # Generate a new request for this message.
      #
      # @param [SmoochApi::Message] message
      #   The incoming message.
      # @param [Hash] nlu_response
      #   The Dialogflow query response.
      # @param [Hash] params
      #   The parsed entities for the request.
      # @param [ActiveRecord::Base] user
      #   The user that sent the message.
      # @param [String] platform
      #   The messaging platform for this request.
      # @param [String] app_id
      #   The ID of the Smooch app handling this request.
      #
      # @return [Heliogram::Smooch::Request]
      #   The generated request.
      private def build_request(message:, nlu_response:, params:, user:, platform:, app_id:)
        Heliogram::Smooch::Request.new(
          user:        user,
          message:     message,
          intent:      nlu_response[:intent],
          action:      nlu_response[:action] || nlu_response[:intent],
          parameters:  params,
          fulfillment: nlu_response[:fulfillment],
          platform:    platform,
          app_id:      app_id
        )
      end

      # Pop the latest raw message from this user's queue.
      #
      # @param user_id [String, Integer]
      #   The ID representing the user on this platform.
      # @param redis_namespace [String]
      #   The redis namespace under which the message to process is nested.
      #
      # @return [Hash]
      #   The latest raw message from this user's queue.
      private def pop_raw_message(user_id, redis_namespace)
        pop_from_queue(
          Heliogram::Smooch::Queues::MessageQueue,
          user_id:   user_id,
          namespace: redis_namespace
        )
      end
    end
  end
end
