# frozen_string_literal: true

module Heliogram
  module Smooch
    class User < ApplicationRecord

      # ---||--------------------------------
      #    || Attributes
      # ===||================================


      # ---||--------------------------------
      #    || Constants
      # ===||================================


      # ---||--------------------------------
      #    || Includes and Extensions
      # ===||================================


      # ---||--------------------------------
      #    || Associations
      # ===||================================

      # ---||--------------------------------
      #    || Callbacks
      # ===||================================

      after_initialize :set_default_values

      # ---||--------------------------------
      #    || Validations
      # ===||================================


      # ---||--------------------------------
      #    || Scopes
      # ===||================================


      # ---||--------------------------------
      #    || Other
      # ===||================================


      # ---||--------------------------------
      #    || Class Methods
      # ===||================================


      # ---||--------------------------------
      #    || Instance Methods
      # ===||================================

      # @return [Search] The user's current search.
      def search
        searches.last
      end

      # Set the conversation state.
      #
      # @param [String] new_conversation_state
      #   The new conversation state to set.
      #
      # @return [void]
      def update_conversation_state!(new_state)
        update_attributes(conversation_state: new_state)
      end

      # Method to update user's context
      #
      # @param [Hash] new_context
      #   The context key and value to set.
      #
      # @return [void]
      def update_context!(new_context)
        update_attributes(context: self[:context].merge(new_context))
      end

      #########
      protected
      #########


      #######
      private
      #######

      # Initialize default user values.
      def set_default_values
        self.api_ai_session_id = SecureRandom.uuid
      end
    end
  end
end
