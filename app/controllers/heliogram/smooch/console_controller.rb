# frozen_string_literal: true

module Heliogram
  module Smooch
    # Smooch test web console.
    class ConsoleController < ApplicationController
      def console
        @bot = Heliogram::Smooch.bots[params[:app_id]]
      end
    end
  end
end
