# frozen_string_literal: true

module Heliogram
  module Smooch
    # Handles inbound Smooch webhooks.
    class WebhooksController < Heliogram::Smooch::ApplicationController
      # Ensure SSL requests if this is a production environment.
      force_ssl if Rails.env.production?

      # Don't check for a CSRF token.
      skip_before_action :verify_authenticity_token

      # Rescue from exceptions.
      rescue_from Exceptions::BotNotFoundError, with: :bad_request
      rescue_from Exceptions::WebhookAuthenticationError, with: :bad_request

      # Handle webhooks.
      def webhook
        # Check the authenticity of the request.
        check_authenticity

        # Before we do anything else, indicate that the message was received.
        # TODO: Determine how to send Business Read Receipt
        # https://docs.smooch.io/rest/#conversation-activity

        # Then send a typing indicator. We're thinking.
        bot.send_typing_indicator(user_id)

        # If this is the beginning of the conversation, trigger the bot's intro
        # payload as a postback request.
        if params.dig(:webhook, :trigger) == 'conversation:start'
          bot.queue_postback(
            {
              'action' => {
                'text'    => 'intro',
                'payload' => JSON.generate(bot.intro_payload),
                'type'    => 'postback'
              }
            },
            params[:source][:type],
            app_id,
            user_id
          )
        # ...otherwise, handle the user messages or postbacks.
        else
          # Add each of the messages to the sender's message queue.
          params[:messages]&.each do |message|
            bot.queue_message(message, app_id)
          end

          # Add each of the postbacks to the sender's postback queue.
          params[:postbacks]&.each do |postback|
            platform = postback['source']['type']

            bot.queue_postback(postback, platform, app_id, user_id)
          end
        end

        # Respond 200 OK.
        head :ok
      end

      private

      # @return [String]
      #   The Smooch app ID for this request.
      def app_id
        @app_id = params.dig(:app, :_id)
      end

      # @return [String]
      #   The Smooch user ID for this request.
      def user_id
        @app_id = params.dig(:appUser, :_id)
      end

      # Check the authenticity of the webhook request.
      def check_authenticity
        # Determine if the request has the correct secret.
        request_authentic =
          (request.headers['X-API-Key'] == bot.webhook_secret)

        # Raise an error if the request isn't authentic.
        raise Exceptions::WebhookAuthenticationError unless request_authentic
      end

      # @return [Bot]
      #   The `Bot` matching this request's app ID.
      def bot
        @bot ||= Heliogram::Smooch.bots[app_id]

        # Raise an error if no bot can be found that matches this Smooch app ID.
        raise Exceptions::BotNotFoundError unless @bot

        @bot
      end
    end
  end
end
