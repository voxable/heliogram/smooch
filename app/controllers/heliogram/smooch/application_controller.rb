module Heliogram
  module Smooch
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :exception

      layout 'heliogram/smooch/application'

      private

      # Render a bad request error.
      #
      # @param exception [StandardError]
      #   The exception being handled.
      #
      # @return [void]
      def bad_request(exception)
        logger.error exception.message
        logger.error Rails.backtrace_cleaner.clean(exception.backtrace)

        head :bad_request
      end
    end
  end
end
