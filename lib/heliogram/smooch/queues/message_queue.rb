# frozen_string_literal: true

module Heliogram
  module Smooch
    module Queues
      class MessageQueue < Heliogram::Queue
        MESSAGES_QUEUE_KEY_PORTION = 'smooch:messages'

        #
        def initialize(user_id:, namespace:)
          key = message_key(
            user_id: user_id,
            namespace: namespace,
            key_portion: MESSAGES_QUEUE_KEY_PORTION
          )
        end
      end
    end
  end
end
