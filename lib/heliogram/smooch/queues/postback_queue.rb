# frozen_string_literal: true

module Heliogram
  module Smooch
    module Queues
      # Enables temporary storage of an ordered list of unprocessed
      # postbacks for a user.
      class PostbackQueue < Heliogram::Queue
        POSTBACKS_QUEUE_KEY_PORTION = 'smooch:postbacks'

        def initialize(user_id: nil, namespace: nil)
          key = message_key(
            user_id: user_id,
            namespace: namespace,
            key_portion: POSTBACKS_QUEUE_KEY_PORTION
          )

          super(key)
        end
      end
    end
  end
end
