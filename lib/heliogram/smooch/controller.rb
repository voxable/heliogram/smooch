# frozen_string_literal: true

module Heliogram
  module Smooch
    class Controller < Heliogram::Controller
      # Send a message back to the user. It's possible to either pass a string,
      # which will be delivered as a text message, or a chunk and its context:
      #
      #   respond Chunks::ConfirmBookingSuccess, {time: Time.now}
      #
      #   respond 'Sounds good!'
      # TODO: Document these params
      def respond(*args)
        # If we're attempting to send back a simple text message...
        if args.first.is_a?(String)
          # ...deliver the message.

          message_text = args.first

          bot.send_text_message(message_text, user.smooch_id)

        # If we're attempting to deliver a chunk...
        elsif args.first.is_a?(Class)
          # ....deliver the chunk
          chunk_class = args[0]
          chunk_context = args[1] || {}

          chunk_class.new(
            recipient_id: user.smooch_id,
            bot: bot,
            context: chunk_context,
            platform: @request.platform
          ).deliver
        end
      end
      alias_method :reply, :respond
      alias_method :ask, :respond
      alias_method :respond_with, :respond


      # Show a typing indicator to the user.
      def show_typing
        bot.send_typing_indicator(user.smooch_id)
      end

      # @return [Heliogram::Smooch::Bot]
      #   The bot handling this request.
      def bot
        @bot ||= Heliogram::Smooch.bots[request.app_id]

        # Raise an error if no bot can be found that matches this Smooch app ID.
        raise Exceptions::BotNotFoundError unless @bot

        @bot
      end
    end
  end
end
