# frozen_string_literal: true

module Heliogram
  module Smooch
    module Analytics
      # Base analytics API client.
      class ApiClient
        include HTTParty

        attr_accessor :intent, :text, :not_handled

        # @return [ApiClient]
        def initialize(params = {})
          @api_key = params.fetch(:api_key, nil)
        end

        protected

        # Rescue errors from HTTParty
        #
        # @return [void]
        def catch_errors
          begin
            @response = yield
          rescue *Exceptions::HTTP_ERRORS => e
            connection_errors(e)
          end
        end

        # Logs connections errors rescued, does not raise error
        # Errors to be captured by preferred error tracker
        #
        # @param [error] e
        #   Error captured
        #
        # @return [void]
        def connection_errors(e)
          Rails.logger.error 'error with Chatbase API request:'
          Rails.logger.error e
          Rails.logger.error e.backtrace.join("\n")
        end

        # Generate the proper request options for a JSON request body.
        #
        # @param [Hash] body
        #   The request body.
        #
        # @return [Hash]
        #   The newly generated JSON body with headers.
        def json_body(body)
          {
            body: body.to_json,
            headers: { 'Content-Type' => 'application/json' }
          }
        end


        # Serialize the message object.
        #
        # @param [Smooch::Message|Hash]
        #   The message object.
        #
        # @return [String|Hash]
        #   The serialized message.
        def serialize_message(message)
          if (message.respond_to?(:type) && message.type == Chunk::MessageTypes::TEXT) ||
                message.respond_to?(:text)
            message.text ? message.text : 'JSON'
          else
            message.to_hash
          end
        end

        # Properly format timestamp.
        #
        # @param [Float] timestamp
        #   The epoch timestamp to format.
        #
        # @return [Integer]
        #   The properly formatted epoch timestamp.
        def serialize_timestamp(timestamp)
          return unless timestamp

          (timestamp * 1000).to_i
        end
      end
    end
  end
end
