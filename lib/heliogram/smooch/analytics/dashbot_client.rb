# frozen_string_literal: true

module Heliogram
  module Smooch
    module Analytics
      # Client used to send metrics to Dashbot.
      class DashbotClient < ApiClient
        base_uri 'https://tracker.dashbot.io'

        # Sends message sent by user to Dashbot.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch message to send.
        # @param [String] intent
        #   The recognized intent for this user message.
        # @param [Hash] entities
        #   The extracted entities for this user message.
        # @param [Boolean] not_handled
        #   true if the message was not recognized, false otherwise.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        # @param [Float] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [void]
        API_VERSION = "10.1.1-rest"

        def record_message(message:, intent:, entities: {}, not_handled:, user_id:, platform: nil, timestamp: nil)
          return unless @api_key

          # Format for Chatbase Facebook API
          message_received = serialize_user_message(
            message:     message,
            intent:      intent,
            entities:    entities,
            not_handled: not_handled,
            user_id:     user_id,
            platform:    platform,
            timestamp:   timestamp
          )

          # Post to Chatbase
          catch_errors {
            result = self.class.post(
              "/track?platform=universal&v=#{API_VERSION}&type=incoming&apiKey=#{@api_key}",
              json_body(message_received)
            )

            result
          }
        end

        # Sends postback sent by user to Dashbot.
        #
        # @param [SmoochApi::MessagePost] postback
        #   The Smooch postback to send.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        #
        # @return [void]
        def record_postback(postback:, user_id:, platform:)
          record_message(
            message:     postback,
            intent:      postback['action'],
            entities:    (postback['params'] || postback['parameters']),
            not_handled: false,
            user_id:     user_id,
            platform:    platform
          )
        end

        # Sends reply sent by bot to Dashbot.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch response sent by the bot.
        # @param [String] user_id
        #   The Smooch user ID of the recipient.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        # @param [Float] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [void]
        def record_reply(message:, user_id:, platform:, timestamp:)
          message_sent = serialize_bot_message(
            message:   message,
            user_id:   user_id,
            timestamp: timestamp
          )

          # Post to Chatbase
          catch_errors {
            result = self.class.post(
              "/track?platform=universal&v=#{API_VERSION}&type=outgoing&apiKey=#{@api_key}",
              json_body(message_sent)
            )

            result
          }
        end

        private

        # Format sent message data for Chatbase API.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch message to send.
        # @param [String] user_id
        #   The Smooch user ID of the recipient.
        # @param [Float] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [Hash]
        #   Message data formatted for Chatbase.
        def serialize_bot_message(message:, user_id:, timestamp:)
          # TODO: Need to be base class methods.

          # We don't record platform for Dashbot, as everything is under the
          # "universal" platform. Otherwise, we'd have to convert Messenger
          # messages to the FB format. (could also use extra platformJson
          # fields)
          {
            dashbot_timestamp: timestamp,
            userId:            user_id,
            text:              serialize_message(message),
            platformJson:      message.to_hash
          }
        end

        # Formats received message data for Dashbot API.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch message to send.
        # @param [String] intent
        #   The recognized intent for this user message.
        # @param [Hash] entities
        #   The extracted entities for this user message.
        # @param [Boolean] not_handled
        #   true if the message was not recognized, false otherwise.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        # @param [Float] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [Hash]
        #   Message data formatted for Chatbase
        def serialize_user_message(message:, intent:, entities:, not_handled:, user_id:, platform:, timestamp: nil)
          # Properly format the entities object.
          inputs = {}
          inputs =
            entities&.map { |name, value|
              {
                name:  name,
                value: value
              }
            }

          {
            dashbot_timestamp: serialize_timestamp(timestamp),
            userId:            user_id,
            text:              serialize_message(message),
            platformJson:      message.to_hash,
            intent:            {
              name:   intent,
              inputs: inputs
            },
            not_handled:       not_handled
          }
        end
      end
    end
  end
end

