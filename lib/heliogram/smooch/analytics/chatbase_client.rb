# frozen_string_literal: true

module Heliogram
  module Smooch
    module Analytics
      # Client used to send metrics to Chatbase.
      class ChatbaseClient < ApiClient
        base_uri 'https://chatbase.com/api/'

        # Sends message sent by user to Chatbase.
        #
        # @param [SmoochApi::MessagePost|Hash] message
        #   The Smooch message to send.
        # @param [String] intent
        #   The recognized intent for this user message.
        # @param [Hash] entities
        #   The extracted entities for this user message.
        # @param [Boolean] not_handled
        #   true if the message was not recognized, false otherwise.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        # @param [Float] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [void]
        def record_message(message:, intent:, entities: [], not_handled:, user_id:, platform:, timestamp: nil)
          return unless @api_key

          # Format for Chatbase Facebook API
          message_received = serialize_user_message(
            message:     message,
            intent:      intent,
            not_handled: not_handled,
            user_id:     user_id,
            platform:    platform,
            timestamp:   timestamp
          )

          # Post to Chatbase
          catch_errors {
            result = self.class.post(
              '/message',
              json_body(message_received)
            )

            result
          }
        end

        # Sends postback sent by user to Chatbase.
        #
        # @param [SmoochApi::MessagePost] postback
        #   The Smooch postback to send.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        #
        # @return [void]
        def record_postback(postback:, user_id:, platform:)
          record_message(
            message:     postback,
            intent:      postback['action'],
            not_handled: false,
            user_id:     user_id,
            platform:    platform
          )
        end

        # Sends reply sent by bot to Chatbase.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch response sent by the bot.
        # @param [String] user_id
        #   The Smooch user ID of the recipient.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        # @param [Float] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [void]
        def record_reply(message:, user_id:, platform:, timestamp:)
          # Format for Chatbase API
          message_sent = serialize_bot_message(
            message:   message,
            user_id:   user_id,
            platform:  platform,
            timestamp: timestamp
          )

          # Post to Chatbase
          catch_errors {
            result = self.class.post(
              '/message',
              json_body(message_sent)
            )

            result
          }
        end

        private

        # Formats received message data for Chatbase API.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch message to send.
        # @param [String] intent
        #   The recognized intent for this user message.
        # @param [Boolean] not_handled
        #   true if the message was not recognized, false otherwise.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        # @param [Float] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [Hash]
        #   Message data formatted for Chatbase
        def serialize_user_message(message:, intent:, not_handled:, user_id:, platform:, timestamp: nil)
          # TODO: Need to be base class methods.
          serialized_message = message.respond_to?(:text) ? message.text : message.to_hash

          {
            api_key:     @api_key,
            time_stamp:  serialize_timestamp(timestamp),
            type:        'user',
            user_id:     user_id,
            platform:    platform,
            message:     serialized_message,
            intent:      intent,
            not_handled: not_handled
          }
        end

        # Format sent message data for Chatbase API.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch message to send.
        # @param [String] user_id
        #   The Smooch user ID of the recipient.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        # @param [Float] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [Hash]
        #   Message data formatted for Chatbase.
        def serialize_bot_message(message:, user_id:, platform:, timestamp:)
          # Determine the message body.
          message =
            message.type == Chunk::MessageTypes::TEXT ? message.text : message.to_json

          {
            api_key:    @api_key,
            time_stamp: timestamp,
            type:       'agent',
            user_id:    user_id,
            platform:   platform,
            message:    message
          }
        end
      end
    end
  end
end

