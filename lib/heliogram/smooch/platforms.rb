# frozen_string_literal: true

module Heliogram
  module Smooch
    # List of supported Smooch platforms.
    class Platforms
      WEB       = 'web'
      IOS       = 'ios'
      ANDROID   = 'android'
      MESSENGER = 'messenger'
      VIBER     = 'viber'
      TELEGRAM  = 'telegram'
      WECHAT    = 'wechat'
      LINE      = 'line'
      TWILIO    = 'twilio'
      OTHER     = 'other'
    end
  end
end
