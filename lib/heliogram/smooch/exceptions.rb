# frozen_string_literal: true

module Heliogram
  module Smooch
    # Custom exception classes.
    module Exceptions
      # @const All HTTP error classes
      HTTP_ERRORS = [
        EOFError,
        Errno::ECONNRESET,
        Errno::EINVAL,
        Net::HTTPBadResponse,
        Net::HTTPHeaderSyntaxError,
        Net::ProtocolError,
        Timeout::Error
      ]

      # Raised when no bots are listed in the directory for this app ID.
      class BotNotFoundError < StandardError

        #
        def initialize(msg = 'No Bot class found matching this app ID'); end
      end

      # Raised when webhook request's authenticity can't be verified.
      class WebhookAuthenticationError < StandardError

        def initialize(msg = 'Webhook request authenticity check failed'); end
      end
    end
  end
end
