# frozen_string_literal: true

require_relative '../../../app/models/heliogram/smooch/application_record'
require_relative '../../../app/models/heliogram/smooch/user'

module Heliogram
  module Errors
    # Error thrown when no router class exists.
    class NoRouterClassExistsError < StandardError
      #
      def initialize
        super("No Router class exists for this bot. Define a rested class Router, or set a router in this bot's initializer.")
      end
    end
  end

  module Smooch
    module Roles
      APP_MAKER = 'appMaker'
    end

    class Bot
      # TODO: Create custom configuration class with dry-struct validation.
      extend Dry::Configurable

      # TODO: High - document these settings
      setting :jwt,                 reader: true
      setting :app_id,              reader: true
      setting :console_title,       reader: true
      setting :webhook_secret,      reader: true
      setting :user_class,  Heliogram::Smooch::User, reader: true
      setting :router
      setting :mapper_chunk_class,  reader: true
      setting :intro_payload,       reader: true

      # The delay (in seconds) between messages, when sending multiple simultaneously.
      # Helps to resolve issues with quick-reply messages not rendering in the
      # correct order.
      setting :message_delay,       0.1, reader: true

      # API key for Chatbase analytics service.
      setting :chatbase_api_key,    reader: true
      # API key for Dashbot analytics service.
      setting :dashbot_api_key,     reader: true

      # If true, automatically adds message platform to chunk context as :platform.
      setting :platform_in_context, false, reader: true

      class << self
        # Initialize the bot.
        def init!
          # Add this `Bot` to the directory of running bots.
          Heliogram::Smooch.bots[app_id] = self
        end

        # @return [SmoochApi::ApiClient]
        #   A Smooch API client for this bot.
        def api_client
          # Create an API client configuration.
          unless @api_client_config
            @api_client_config = SmoochApi::Configuration.new
            @api_client_config.api_key['Authorization'] = jwt
            @api_client_config.api_key_prefix['Authorization'] = 'Bearer'
          end

          # Create an API client with the configuration for this bot.
          @api_client ||= SmoochApi::ApiClient.new(@api_client_config)
        end

        # @return [SmoochApi::ConversationApi]
        #   A Smooch Conversation API client.
        def conversation_api_client
          @conversation_api_client ||= SmoochApi::ConversationApi.new(api_client)
        end

        # Queue a message for processing.
        #
        # @param [ActionController::Parameters] message
        #   The Smooch message to be queued.
        # @see https://docs.smooch.io/rest/#message-schema
        # @param [String] app_id
        #   The ID of the Smooch app handling this message.
        #
        # @return [void]
        def queue_message(message, app_id)
          # Store message on this user's queue of unprocessed messages.
          user_id = message['authorId']

          Queues::MessageQueue
            .new(user_id: user_id, namespace: redis_namespace)
            .push(message)

          # Queue message for processing.
          Heliogram::Smooch::MessageWorker.perform_async(
            user_id, app_id, redis_namespace
          )
        end

        # Queue a postback for processing.
        #
        # @param [ActionController::Parameters] postback
        #   The Smooch postback event to be queued.
        # @see https://docs.smooch.io/rest/#postback-events
        # @param [String] platform
        #   The platform for this postback.
        # @param [String] app_id
        #   The ID of the Smooch app handling this message.
        # @param [String] user_id
        #   The Smooch user ID of the author of the message.
        #
        # @return [void]
        def queue_postback(postback, platform, app_id, user_id)
          # Parse the postback payload as JSON
          payload = JSON.parse(postback['action']['payload'])

          # Store the transformed postback on the queue
          Heliogram::Smooch::Queues::PostbackQueue
            .new(user_id: user_id, namespace: redis_namespace)
            .push(payload)

          # Queue postback for processing.
          Heliogram::Smooch::PostbackWorker.perform_async(
            user_id, app_id, redis_namespace, platform
          )
        end

        # @return [Class] The bot's router class.
        def router
          @router ||= const_get(:Router) || config.router
        rescue LoadError
          raise Errors::NoRouterClassExistsError.new
        end

        # Send a typing indicator to a user.
        #
        # @param [String] user_id
        #   The Smooch user ID.
        #
        # @return [void]
        def send_typing_indicator(user_id)
          # TODO: Add name and avatar_url
          typing_activity_trigger_body = SmoochApi::TypingActivityTrigger.new(
            role: Roles::APP_MAKER,
            type: 'typing:start'
          )

          begin
            conversation_api_client.trigger_typing_activity(
              app_id, user_id, typing_activity_trigger_body
            )
          rescue StandardError => e
            log_error(e)
          end
        end

        # Send a simple text message to a user.
        #
        # @param [String] message
        #   The message to send.
        # @param [String] user_id
        #   The user ID to send the message to.
        #
        # @return [void]
        def send_text_message(message, user_id)
          text_message_body = SmoochApi::MessagePost.new(
            role: Roles::APP_MAKER,
            type: Chunk::MessageTypes::TEXT,
            text: message
          )

          conversation_api_client.post_message(
            app_id, user_id, text_message_body
          )
        end

        # Send a message to a user.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch message to send.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        #
        # @return [void]
        def send_message(message, user_id, platform)
          retries = 0

          begin
            conversation_api_client.post_message(
              app_id, user_id, message
            )
          rescue SmoochApi::ApiError => e
            # Retry the call.
            if retries < 3
              retries += 1

              sleep retries * 0.5
              Rails.logger.info 'RETRYING'
              retry
            else
              smooch_error = JSON.parse(e.response_body).dig('error', 'description').truncate(100)
              Appsignal.set_error(
                e,
                {
                  smooch_error:  smooch_error,
                  response:      message.to_s.truncate(100)
                },
                'smooch'
              )

              Rails.logger.error('Smooch API Error')
              Rails.logger.error(smooch_error)
              Rails.logger.error('Message')
              Rails.logger.error(message.to_s)

              log_error(e)
            end
          end

          record_reply(message: message, user_id: user_id, platform: platform)
        end

        # Record an incoming message from the user with analytics services.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch message to send.
        # @param [String] intent
        #   The recognized intent for this user message.
        # @param [Hash] entities
        #   The extracted entities for this user message.
        # @param [Boolean] not_handled
        #   true if the message was not recognized, false otherwise.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        # @param [Integer] timestamp
        #   The epoch timestamp for this message.
        #
        # @return [void]
        def record_message(message:, intent:, entities:, not_handled:, user_id:, platform:, timestamp:)
          begin
            analytics_api_clients.map { |client|
              Thread.new {
                client.record_message(
                  message:     message,
                  intent:      intent,
                  entities:    entities,
                  not_handled: not_handled,
                  user_id:     user_id,
                  platform:    platform,
                  timestamp:   timestamp
                )
              }
            }.map!(&:value)
          rescue StandardError => e
            log_error(e)
          end
        end

        # Record an incoming postback from the user with analytics services.
        #
        # @param [Hash] postback
        #   The Smooch postback to send.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        #
        # @return [void]
        def record_postback(postback:, user_id:, platform:)
          begin
            analytics_api_clients.map { |client|
              Thread.new {
                client.record_postback(
                  postback:  postback,
                  user_id:   user_id,
                  platform:  platform
                )
              }
            }.map!(&:value)
          rescue StandardError => e
            log_error(e)
          end
        end

        # @return [String]
        #   A redis namespace for this bot.
        private def redis_namespace
          "smooch:#{app_id}"
        end

        # Record a sent message with analytics services.
        #
        # @param [SmoochApi::MessagePost] message
        #   The Smooch message to send.
        # @param [String] user_id
        #   The user ID to send the message to.
        # @param [String] platform
        #   The messaging platform that this message is being sent via.
        #
        # @return [void]
        private def record_reply(message:, user_id:, platform:)
          begin
            timestamp = DateTime.now.strftime('%Q').to_i

            analytics_api_clients.map { |client|
              Thread.new {
                client.record_reply(
                  message:   message,
                  user_id:   user_id,
                  platform:  platform,
                  timestamp: timestamp
                )
              }
            }.map!(&:value)
          rescue StandardError => e
            log_error(e)
          end
        end

        # Build an array of analytics services API clients to use with this bot.
        #
        # @return [Array]
        #   The API clients to use to log replies.
        private def analytics_api_clients
          return @analytics_api_clients if @analytics_api_clients

          @analytics_api_clients = []

          add_chatbase_client
          add_dashbot_client

          @analytics_api_clients
        end

        # Add a Chatbase API client to the list of analytics clients, if a key
        # is present.
        #
        # @return [void]
        private def add_chatbase_client
          return unless chatbase_api_key

          @analytics_api_clients << Analytics::ChatbaseClient.new(api_key: chatbase_api_key)
        end

        # Add a Dashbot API client to the list of analytics clients, if a key
        # is present.
        #
        # @return [void]
        private def add_dashbot_client
          return unless dashbot_api_key

          @analytics_api_clients << Analytics::DashbotClient.new(api_key: dashbot_api_key)
        end

        # Log an error.
        #
        # @param [Error] error
        #   The error to log.
        #
        # @return [void]
        private def log_error(error)
          Rails.logger.error error.message
          Rails.logger.error error.backtrace.join("\n")
        end
      end
    end
  end
end
