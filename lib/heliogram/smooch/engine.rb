# frozen_string_literal: true

module Heliogram
  module Smooch
    class Engine < ::Rails::Engine
      AUTOLOAD_PATHS = Dir.glob(File.join(Heliogram::Smooch::Engine.root, 'lib', '**/*'))
      config.autoload_paths   += AUTOLOAD_PATHS
      config.eager_load_paths += AUTOLOAD_PATHS

      isolate_namespace Heliogram::Smooch
    end
  end
end
