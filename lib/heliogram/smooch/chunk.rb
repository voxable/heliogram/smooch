# frozen_string_literal: true

module Heliogram
  module Smooch
    class Chunk
      # Initialize a new chunk.
      #
      # @param [String] recipient_id
      #   The Smooch user ID of the recipient.
      # @param [Heliogram::Smooch::Bot] bot
      #   The bot responsible for delivering the message.
      # @param [Hash] context
      #   The execution context for this chunk.
      # @param [String] platform
      #   The platform for this message.
      #
      # @return [Heliogram::Smooch::Chunk]
      #   The new chunk.
      def initialize(recipient_id: nil, bot: nil, context: {}, platform: nil)
        @recipient_id = recipient_id
        @bot          = bot

        # Add the platform to chunk context, if requested.
        context.merge!(platform: platform) if bot.platform_in_context

        @context      = HashWithIndifferentAccess.new(context)
        @platform     = platform
      end

      # Acceptable Smooch action types.
      module ActionTypes
        LINK     = 'link'
        POSTBACK = 'postback'
        REPLY    = 'reply'
        SHARE    = 'share'
      end

      # Acceptable Smooch message types.
      module MessageTypes
        CAROUSEL = 'carousel'
        IMAGE    = 'image'
        TEXT     = 'text'
      end

      class << self
        # Instantiate class instance variables with default values.
        def inherited(subclass)
          subclass.deliverables = []
        end

        attr_accessor :deliverables

        # Add an action to a message.
        #
        # @param [String] text
        #   The button label.
        #
        # @return [void]
        def action(text, options = {})
          attributes = {
            text: text
          }

          # If a `to` option is present, assume this is a postback link to another chunk.
          if options[:to]
            attributes[:type] = ActionTypes::POSTBACK
            attributes[:payload] =
              JSON.generate({
                action: Heliogram::InternalActions::DISPLAY_CHUNK,
                parameters: {
                  chunk: options[:to].to_s
                }
              })
          # If a `url` option is present, assume this is a webview link button.
          elsif url = options[:uri] || options[:url]
            attributes[:type] = ActionTypes::LINK

            # Append Abby utm params
            attributes[:uri] = url
          elsif options[:payload]
            attributes[:type] = ActionTypes::POSTBACK

            # Encode the payload hash as JSON.
            attributes[:payload] = JSON.generate(options[:payload])
          end

          # If building a gallery card...
          if @card
            # ...add the button to the card.
            @card[:actions] << SmoochApi::Action.new(attributes)
          else
            # ...otherwise, add it to the latest deliverable.
            add_action(attributes)
          end
        end
        alias button action

        # Add a card to a message carousel.
        def card
          @card = {
            actions: []
          }
          yield
          @cards << @card
          @card = nil
        end

        # Add a message carousel.
        def carousel
          @cards = []

          yield

          # TODO: Throw an error if there are too many cards present.

          add_message(
            type:  MessageTypes::CAROUSEL,
            items: @cards
          )
        end
        alias gallery carousel

        # Add a description to a carousel card.
        #
        # @param [String] text
        #   The description for the card.
        #
        # @return [void]
        def description(text)
          # TODO: High - error if > 128 characters
          @card[:description] = text
        end
        alias subtitle description

        # Dynamically evaluate the chunk with the context passed in at
        # instantiation.
        def dynamic(&block)
          @deliverables << block
        end

        # Send an image message.
        #
        # @param [String] url
        #   The URL to the image.
        #
        # @return [void]
        def image(url)
          attachment(MessageTypes::IMAGE, url)
        end

        # Add media to a carousel card.
        def media_url(url, options = {})
          return nil if url.blank?

          options.with_defaults!(size: 'compact')

          if options.has_key?(:host)
            @card[:mediaUrl] =
              ActionController::Base.helpers.image_url(url, options)
          else
            @card[:mediaUrl] = url
          end

          return unless url

          @card[:mediaType] =
            options[:media_type] || media_type(url)
        end
        alias image_url media_url

        # Generate a Messenger ref link.
        #
        # @param [String] page_id
        #   The Facebook page ID that this bot is deployed on.
        # @param [Hash] payload
        #   The payload of the ref link.
        #
        # @return [void]
        def messenger_ref_link(page_id, payload)
          "http://m.me/#{page_id}?ref=#{CGI.escape(payload.to_json)}"
        end

        # Add several reply actions to message by linking directly to passed
        # chunks.
        def reply_actions(*classes)
          classes.each do |klass|
            quick_reply klass.instance_variable_get(:@label), to: klass
          end
        end
        alias quick_replies reply_actions

        # Add a quick reply to the chunk.
        #
        # @param [String] text
        #   The label for this quick reply button.
        # @param [Hash] options
        #   The options for this quick reply button.
        #
        # @return [void]
        def reply_action(text, options = {})
          reply_action_content = {
            type: ActionTypes::REPLY,
            text: text
          }

          # If image_url is specified include the url or asset path
          if options[:image_path]
            reply_action_content[:iconUrl] =
              ActionController::Base
                .helpers
                .image_url(options[:image_path], host: options[:host])
          else
            reply_action_content[:iconUrl] =
              (options[:image_url] || options[:icon_url])
          end

          # If a `to` option is present, assume this is a postback link to
          # another chunk.
          if options[:to]
            reply_action_content[:payload] =
              JSON.generate(
                action:     Heliogram::InternalActions::DISPLAY_CHUNK,
                parameters: {
                  chunk: options[:to].to_s
                }
              )
          # Otherwise, just take the payload as passed.
          else
            reply_action_content[:payload] = JSON.generate(options[:payload])
          end

          # TODO: High - Add argument error if no @deliverables have been set
          # or if @deliverables is a proc (which means the QR's need to be
          # in the dynamic block)

          add_action(reply_action_content)
        end
        alias quick_reply reply_action

        # Add a share button to a card.
        #
        # @see https://docs.smooch.io/rest/#message-actions
        # @see https://developers.facebook.com/docs/messenger-platform/send-api-reference/share-button
        #
        # @return [void]
        def share_button(&block)
          button_options = {
            type: ActionTypes::SHARE
          }

          # If a chunk is passed, add as share_contents option.
          if block_given?
            original_gallery = @gallery.clone
            original_card = @card.clone

            @gallery = {
              cards: [],
              attachment: {
                type: 'template',
                payload: {
                  template_type: 'generic',
                  elements: []
                }
              }
            }

            card(&block)

            @gallery[:attachment][:payload][:elements] = @gallery.delete(:cards)

            button_options[:metadata] = {
              share_contents: @gallery.clone
            }

            @gallery = original_gallery
            @card = original_card
          end

          # Add the share button to the current card.
          @card[:actions] << SmoochApi::Action.new(button_options)
        end

        # Add text message to chunk.
        #
        # @param [String, Array] message
        #   Message to be delivered.
        #   If message is an array, will deliver one at random
        #
        # @return [void]
        def text(message)
          # Choose a random message if `message` is an array.
          message = message.sample if message.respond_to?(:sample)

          add_message(
            type: MessageTypes::TEXT,
            text: message
          )
        end

        # Add a title to a carousel card.
        #
        # @param [String] text
        #   The title for the card.
        #
        # @return [void]
        def title(text)
          @card[:title] = text
        end

        # Send a video message.
        #
        # @param [String] url
        #   The URL to the video.
        #
        # @return [void]
        # def video(url)
        #  attachment('video', url)
        # end

        # Create a new Smooch message and add it to this chunk's list
        # of deliverables.
        #
        # @param [Hash] attributes
        #   The Smooch message attributes.
        #
        # @return [SmoochApi::MessagePost]
        #   The Smooch message.
        private def add_message(attributes = {})
          message_attributes = attributes.with_defaults(
            role: 'appMaker'
          )

          @deliverables << SmoochApi::MessagePost.new(message_attributes)
        end

        # Create a new media message and add it to this chunk's list of
        # deliverables.
        private def attachment(type, url)
          add_message(
            type:     type,
            mediaUrl: url
          )
        end

        # Add an action to the last deliverable.
        #
        # @param [Hash] action_attributes
        #   The attributes of the action to add.
        #
        # @return [void]
        private def add_action(action_attributes)
          @deliverables.last.actions = [] unless @deliverables.last.actions

          @deliverables.last.actions << SmoochApi::Action.new(action_attributes)
        end

        # Generate a MIME type based on the image URL.
        #
        # @param [String] url
        #   The URL for the image.
        #
        # @return [String]
        #   The MIME type for the image.
        private def media_type(url)
          media_extension = url.split('.').last

          if media_extension.length > 4
            # Attempt to determine mime type by fetching the image URL.
            begin
              ImageSpec.new(url)&.content_type
            rescue StandardError => e
              Rails.logger.error(e)
              nil
            end
          elsif media_extension == 'jpg'
            'image/jpeg'
          else
            "image/#{media_extension}"
          end
        end
      end

      # Deliver the chunk to the recipient.
      def deliver
        Sidekiq::Logging.logger.info 'DELIVERABLES' unless Rails.env.production?


        self.class.deliverables.each_with_index do |deliverable, index|
          Sidekiq::Logging.logger.info deliverable.inspect unless Rails.env.production?

          # If another chunk...
          if deliverable.is_a? Class
            # ...deliver the chunk.
            deliverable.new(
              recipient_id: @recipient_id,
              bot:          @bot,
              context:      @context,
              platform:     @platform).deliver
            # If dynamic, then it needs to be evaluated at delivery time.
          elsif deliverable.is_a? Proc
            # Create a `template` anonymous subclass of the chunk class.
            template = Class.new(self.class)
            template.deliverables = []

            # Make some variables available to the chunk,
            # in case they are needed for logging purposes.
            template.class_exec(@recipient_id) do |recipient_id|
              @recipient_id = recipient_id
            end

            template.class_exec(@platform) do |platform|
              @platform = platform
            end

            # Evaluate the dynamic block within it.
            template.class_exec(@context, &deliverable)

            # Deliver the chunk.
            template.new(
              recipient_id: @recipient_id,
              bot:          @bot,
              context:      @context,
              platform:     @platform).deliver

          # Otherwise, it's just a raw message.
          else
            # Deliver the message
            @bot.send_message(
              deliverable, @recipient_id, @platform
            )
          end

          # Pause before sending next message. Prevents errors in message
          # rendering on some platforms when multiple messages are sent in a row
          # (e.g. Messenger).
          delay = (index != self.class.deliverables.size - 1) &&
            (@platform == Platforms::MESSENGER)
          sleep @bot.message_delay if delay
        end
      end
    end
  end
end
