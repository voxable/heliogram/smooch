# frozen_string_literal: true

module Heliogram
  module Smooch
    # Represents an inbound request to a bot. Contains all relevant request information.
    class Request < Heliogram::Request
      # The ID of the Smooch app handling the request.
      attr_accessor :app_id
      # The messaging platform for this request.
      attr_accessor :platform

      # TODO: Use dry-initializer
      def initialize(options = {})
        @app_id = options.fetch(:app_id)
        @platform = options.fetch(:platform)
        super(options)
      end
    end
  end
end

