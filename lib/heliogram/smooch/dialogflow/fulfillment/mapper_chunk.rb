# frozen_string_literal: true

module Heliogram
  module Smooch
    module Dialogflow
      module Fulfillment
        class MapperChunk < Heliogram::Smooch::Chunk
          include Fulfillment::Helpers

          dynamic do |context|
            if context[:fulfillment]&.length > 0
              context[:fulfillment].each do |message|
                cards = []
                images = []

                if message.platform.to_s == 'FACEBOOK'

                  # If this isn't a card, render a gallery out of the sequential cards.
                  if (message.card || message.basic_card) && cards.any?
                    render_gallery(cards)
                    cards = []
                  end

                  if message.text
                    render_text_response(message.text.text.first)
                  elsif message.card || message.basic_card
                    cards << message
                  elsif message.image
                    images << message.image
                  end

                  render_gallery(cards) if cards.any?
                  # Render a random image if several specified.
                  image(images.sample) if images.any?
                elsif message.platform.to_s == 'PLATFORM_UNSPECIFIED' && message.text
                  text message.text.text.first
                end
              end
            end

            # @QR
            text '👆'
          end
        end
      end
    end
  end
end
