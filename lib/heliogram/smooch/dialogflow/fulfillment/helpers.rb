# frozen_string_literal: true

module Heliogram
  module Smooch
    module Dialogflow
      module Fulfillment
        module Helpers
          extend ActiveSupport::Concern

          class_methods do
            def render_gallery(messages)
              gallery do
                messages.each do |message|
                  card do
                    image_url message['imageUrl'] if message['imageUrl']

                    title message['title']
                    subtitle message['subtitle']

                    message['buttons'].each do |button_data|
                      button_text = button_data['text']
                      postback = button_data['postback']

                      # TODO: Need a better regex, here.
                      # TODO: Need call_button
                      if postback.match?(/\(\d{3}\)\s\d{3}-\d{4}/)
                      # call_button button_text, number: postback
                      else
                        button button_text, url: postback
                      end
                    end
                  end
                end
              end
            end

            BUTTON_REGEX = /(.*):(https?:\/\/[\S]+)/
            #
            def render_text_response(text_response)
              # Create regex to search for text buttons.
              previous_line = nil

              # For each line in the message.
              text_response.split("\n").reject(&:blank?).each do |line|
                if line.match(BUTTON_REGEX)
                  label = $1
                  url = $2

                  text previous_line

                  button label, url: url
                else
                  # ...otherwise, render the line as a text message.
                  render_previous_line(previous_line)
                end

                previous_line = line
              end

              render_previous_line(previous_line)
            end

            #
            def render_previous_line(previous_line)
              return unless previous_line && !previous_line.match(BUTTON_REGEX)

              text previous_line
            end
          end
        end
      end
    end
  end
end
