module Heliogram
  module Smooch
    module Dialogflow
      module Fulfillment
        # TODO: High - document and test
        class Responder
          #
          def initialize(request)
            @request = request
          end

          #
          def respond
            bot = Heliogram::Smooch.bots[@request.app_id]
            bot.mapper_chunk_class.new(
              recipient_id: @request.user.smooch_id,
              bot:          bot,
              context:      { fulfillment: @request.fulfillment },
              platform:     @request.platform
            ).deliver
          end
        end
      end
    end
  end
end
