# frozen_string_literal: true

require 'dry/configurable'
require 'httparty'
require 'smooch-api'
require 'image_spec'

require_dependency 'heliogram'
require_dependency 'heliogram/smooch/engine'
require 'heliogram/smooch/version'

require_dependency 'heliogram/smooch/analytics/api_client'
require_dependency 'heliogram/smooch/analytics/chatbase_client'
require_dependency 'heliogram/smooch/analytics/dashbot_client'

require_dependency 'heliogram/smooch/queues/message_queue'
require_dependency 'heliogram/smooch/queues/postback_queue'

require_dependency 'heliogram/smooch/bot'
require_dependency 'heliogram/smooch/chunk'
require_dependency 'heliogram/smooch/controller'
require_dependency 'heliogram/smooch/exceptions'
require_dependency 'heliogram/smooch/platforms'
require_dependency 'heliogram/smooch/request'

require_dependency 'heliogram/smooch/dialogflow/fulfillment/helpers'
require_dependency 'heliogram/smooch/dialogflow/fulfillment/mapper_chunk'
require_dependency 'heliogram/smooch/dialogflow/fulfillment/responder'

module Heliogram
  module Smooch
    # @!attribute [Hash]
    #   A directory of running Bots keyed by app ID.
    mattr_accessor :bots do
      {}
    end
  end
end
