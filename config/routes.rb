# frozen_string_literal: true

Heliogram::Smooch::Engine.routes.draw do
  post '/', to: 'webhooks#webhook'

  get '/console', to: 'console#console' if Rails.env.development?
end
