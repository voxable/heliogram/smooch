class AddHeliogramSmoochUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :heliogram_smooch_users do |t|
      t.string :smooch_id, index: true
      t.string :user_id, index: true
      t.datetime :signed_up_at
      t.boolean :credential_required, default: false
      t.string :email
      t.string :given_name
      t.string :surname
      t.string :api_ai_session_id
      t.string :dialogflow_context_name
      t.string :conversation_state

      t.timestamps
    end

    enable_extension 'hstore'

    add_column :heliogram_smooch_users,
               :context,
               :hstore,
               default: {},
               null: false
  end
end
